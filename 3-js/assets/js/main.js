// jQuery
// $(document).ready(function() {
// code
// });

// Vanilla JS

// window.onload = function() {
// code
// };

let botaoMenu;
let botaoVideo;
let botaoModal;
let botaoFechar;
let btnSanfonaUm, btnSanfonaDois, btnSanfonaTres, btnSanfonaQuatro;

function mostraElemento(elemento) {
  if(elemento.classList.contains("-active") === false){
    elemento.classList.add("-active");
  } else {
    elemento.classList.remove("-active");
  }  
}

function mostraVideo(imagem) {
  if(imagem.classList.contains("-inactive") === false){
    imagem.classList.add("-inactive");
  }
}

function mostraFechaModal(modal, body){
  if(modal.classList.contains("modal-wiki-active") === false && body.classList.contains("-overlay") === false){
    modal.classList.add("modal-wiki-active");
    body.classList.add("-overlay");
    return;
  } else if(modal.classList.contains("modal-wiki-active") === true && body.classList.contains("-overlay") === true){
    modal.classList.remove("modal-wiki-active");
    body.classList.remove("-overlay");
    return;
  }
}

window.addEventListener('load', function () {
  botaoMenu = document.getElementById('menu-button');
  botaoMenu.addEventListener('click', function(){
    const menu = document.getElementById('menu');
    mostraElemento(menu);
  });  

  botaoVideo = document.getElementById('video-player');
  botaoVideo.addEventListener('click', function(){
    const imgTag = document.getElementById('video-cover');
    mostraVideo(imgTag);
  });

  const divModal = document.getElementById('modal-wiki');
  const body = document.querySelector('body');

  botaoModal = document.getElementById('button-modal');
  botaoModal.addEventListener('click', function(){
    mostraFechaModal(divModal, body);
    $.get("https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro&explaintext&  redirects=1&titles=Alber%20Einstein", function(einstein){
      $("#modal-wiki").html(einstein);
    });    
  });

  botaoFechar = document.getElementById('close-modal');
  botaoFechar.addEventListener('click', function(){
    mostraFechaModal(divModal, body);
  });

  btnSanfonaUm = document.getElementById('sanfona-one');
  btnSanfonaUm.addEventListener('click', function(){
    mostraElemento(btnSanfonaUm);
  });

  btnSanfonaDois = document.getElementById('sanfona-two');
  btnSanfonaDois.addEventListener('click', function(){
    mostraElemento(btnSanfonaDois);
  });

  btnSanfonaTres = document.getElementById('sanfona-three');
  btnSanfonaTres.addEventListener('click', function(){
    mostraElemento(btnSanfonaTres);
  });

  btnSanfonaQuatro = document.getElementById('sanfona-four');
  btnSanfonaQuatro.addEventListener('click', function(){
    mostraElemento(btnSanfonaQuatro);
  });

});